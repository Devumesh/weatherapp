from django.urls import path
from . import views

urlpatterns = [
    path('', views.weatherApp, name="home"),
    path('new/', views.NewCity.as_view(), name="new"),
]







