# Generated by Django 3.0 on 2020-04-29 16:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('weatherApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='weathermodel',
            options={'verbose_name_plural': 'cities'},
        ),
    ]
