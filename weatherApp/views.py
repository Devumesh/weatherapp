import requests
from django.shortcuts import render
from .models import WeatherModel


# Create your views here.
def weatherApp(request):
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=imperial&appid=df22725cc932473f08c5ff95306698ec'
    
    cities = WeatherModel.objects.all()

    weatherData = []

    for city in cities:

        data = requests.get(url.format(city)).json()
        weather_report ={
            'city': city,
            'temperature': data['main']['temp'],
            'humidity': data['main']['humidity'],
            'description': data['weather'][0]['description'],
            'icon': data['weather'][0]['icon']
        }

        weatherData.append(weather_report)


    context = {'weatherData': weatherData}

    return render(request, 'weather.html', context)


from django.views.generic.edit import CreateView
from django.urls import reverse_lazy

class NewCity(CreateView):
    model = WeatherModel
    template_name = 'newCity.html'
    fields = '__all__'
    success_url = reverse_lazy('home')




